# SPDX-FileCopyrightText: 2020 Intevation GmbH
#
# SPDX-License-Identifier: BSD-3-Clause
"""example of use of the mail_producer module
produced an example encrypted and signed mail and print it to stdout.
Use smtplib for sending your mails/messages.
Authors: Ludwig Reiter <ludwig.reiter@intevation.de>
"""
import os
import shutil
import tempfile
import gpg
from mail_producer import create_encrypted_and_signed_mail


# build a temp gpg home and import keys.
_gpghome = tempfile.mkdtemp(prefix='tmp.gpghome')
os.environ['GNUPGHOME'] = _gpghome


# import requested keys into the keyring
def keyfile(key_name):
    keydir = os.path.join(os.path.dirname(__file__), 'keys')
    return open(os.path.join(keydir, key_name), 'rb')


ctx = gpg.Context()


import_keys = ['test1.sec', 'test1.pub', 'test2.gpg', 'test2.pub']
for key in import_keys:
    with keyfile(key) as fp:
        ctx.key_import(fp)

# now prepare key and ctx for signing.
ctx.signers = [ctx.get_key('5F503EFAC8C89323D54C252591B8CD7E15925678')]

try:
    NO_ATTACHMENTS = None
    recipient_key = ctx.get_key('2D22891B0EA69FBCA1808F2926F69225D182BFCE')
    BODY = """Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
    aliquyam erat, sed diam voluptua. At vero eos et accusam et\n"""
    email = create_encrypted_and_signed_mail(
        "test1.intelmq@example.org",
        "test2@example.org",
        recipient_key,
        "encrypted test mail with signature",
        BODY,
        NO_ATTACHMENTS, ctx)
    print(email)
# clean up GNUPGHOME and remove the temporary _gpghome
finally:
    del os.environ['GNUPGHOME']
    shutil.rmtree(_gpghome, ignore_errors=True)
