<!--
SPDX-FileCopyrightText: 2020 Intevation GmbH

SPDX-License-Identifier: BSD-3-Clause
-->

# README #

This repository includes examples to use python3-gpg. Like sending an OpenPGP/MIME signed mail
with a password-less private key.

### What is this repository for? ###

* Provide Examples
* Save my progress in the gnupg world.

### How do I get set up? ###

* you need to Python 3
* You need to install python3-gpg (on Debian, Ubuntu)

### Contribution guidelines ###

* I'm interested in feedback, if it helps a bit in the way to get into gnupg
  world.

### Who do I talk to? ###

* Ludwig Reiter (Intevation)
